Source: python-drf-spectacular
Section: python
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders:
 Michael Fladischer <fladi@debian.org>,
Build-Depends:
 debhelper-compat (= 13),
 dh-sequence-python3,
 dh-sequence-sphinxdoc,
 furo,
 pybuild-plugin-pyproject,
 python3-all,
 python3-django (>= 2:2.2~),
 python3-django-filters,
 python3-django-oauth-toolkit (>= 1.2.0~),
 python3-django-polymorphic,
 python3-djangorestframework (>= 3.10~),
 python3-inflection,
 python3-jsonschema,
 python3-psycopg2,
 python3-pytest <!nocheck>,
 python3-pytest-django,
 python3-setuptools,
 python3-sphinx,
 python3-uritemplate (>= 3~),
 python3-yaml,
Standards-Version: 4.7.0
Homepage: https://github.com/tfranzel/drf-spectacular/
Vcs-Browser: https://salsa.debian.org/python-team/packages/python-drf-spectacular
Vcs-Git: https://salsa.debian.org/python-team/packages/python-drf-spectacular.git
Rules-Requires-Root: no

Package: python-djangorestframework-spectacular-doc
Section: doc
Architecture: all
Depends:
 ${misc:Depends},
 ${sphinxdoc:Depends},
Multi-Arch: foreign
Description: OpenAPI 3 schema generation for Django REST framework (Documentation)
 Sane and flexible OpenAPI 3 schema generation for Django REST framework. The
 code is a heavily modified fork of the DRF OpenAPI generator, which is/was
 lacking all of the below listed features:
 .
  * Serializers modelled as components (arbitrary nesting and recursion
    supported)
  * Authentication support (DRF natives included, easily extendable)
  * Custom serializer class support
  * Tags extraction
  * Description extraction from docstrings
  * Sane fallbacks where no Serializer is available
  * Sane operation_id naming (based on path)
  * Easy to use hooks for extending the AutoSchema class
  * Optional schema serving with SpectacularAPIView
 .
 This package contains the documentation.

Package: python3-djangorestframework-spectacular
Architecture: all
Depends:
 ${misc:Depends},
 ${python3:Depends},
Suggests:
 python-djangorestframework-spectacular-doc,
Description: OpenAPI 3 schema generation for Django REST framework (Python3 version)
 Sane and flexible OpenAPI 3 schema generation for Django REST framework. The
 code is a heavily modified fork of the DRF OpenAPI generator, which is/was
 lacking all of the below listed features:
 .
  * Serializers modelled as components (arbitrary nesting and recursion
    supported)
  * Authentication support (DRF natives included, easily extendable)
  * Custom serializer class support
  * Tags extraction
  * Description extraction from docstrings
  * Sane fallbacks where no Serializer is available
  * Sane operation_id naming (based on path)
  * Easy to use hooks for extending the AutoSchema class
  * Optional schema serving with SpectacularAPIView
 .
 This package contains the Python 3 version of the library.
